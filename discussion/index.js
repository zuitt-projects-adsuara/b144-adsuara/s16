











































/*Do-While Loop 
At least one code block will be  executed before proceeding to the condition.

Syntax:
  do{
	statement/s;
}while(expression/condition)
*/
let countThree =5;
do{
	console.log("Do-while loop:"+ countThree)
 
    countThree--;
}   while(countThree > 0)
/*Display numbers 1-10 using do-while loop*/

let countFour =1;
do{
	console.log("Do-while loop:" + countFour)
	countFour++;
	}while(countFour <= 10)	


//For Loop - more flexible looping construct 
/* Syntax:
    for(initialization; expression/condition; finalExpression){
	   statement/s;
    }
*/

for(let countFive = 5; countFive > 0; countFive --){
	console.log("For loop:" + countFive);
}

/*let number = Number(prompt("Give Me a Number:"));


for(let numCount = 1; numCount <= number; numCount++){
	console.log("Hello Batch 144");
}*/

let myString = "alex";
/*console.log(myString.length);

console.log(myString[2]);*/

for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

/*
   Array
   elements - represents the values in the array
   index - locatio of values(elements) in the array which starts at index 0.
   elements - a l e x 
   index    - 0 1 2 3

*/

let myName = "Alex"
for (let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u'
	){  // if the letter is the name is a vowel, it will print 3
		console.log(3)
	}else{
		//Will print in the console if character is a non-vowel
		console.log(myName[i])
	}

}

/*Continue and Break statements

  Continue - allows the code to go to the next iteration for the loop 
  without finishing the execution of all the statements in a code block.
  Break - used to terminated the current loop once a match has been found

*/

/*for(let count = 0; count <=20; count++){
	// if remainder is equal to 0
	if count (% 2 === 0){
		// Tells the code to continue to the next iteration of the loop
		continue;
	}
	The current value of number is printed out if the remainder is not equal to 0
	console.log("Continue and Break:" + count);
	// if the current value of count is greater than 10
	if (count > 10){
    // Tells the code to terminate the loop even if the loop condition is still being satisfied. 
		break;
	}
}*/
// count 9 10 11
let name = "Alexandro";

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

   if( name[i].toLowerCase()  === "a"){
     console.log("Continue to the next iteration");
     continue;
   }
   if(name[i].toLowerCase() === "d"){
   	break;
   }
}
   	